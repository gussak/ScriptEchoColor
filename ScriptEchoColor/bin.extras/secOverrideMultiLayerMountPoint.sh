#!/bin/bash
# Copyright (C) 2016-2022 by Henrique Abdalla
#
# This file is part of ScriptEchoColor.
#
# ScriptEchoColor simplifies Linux terminal text colorizing, formatting 
# and several steps of script coding.
#
# ScriptEchoColor is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# ScriptEchoColor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ScriptEchoColor. If not, see <http://www.gnu.org/licenses/>.
#
# Homepage: http://scriptechocolor.sourceforge.net/
# Project Homepage: https://sourceforge.net/projects/scriptechocolor/

source <(secinit)

: ${strEnvVarUserCanModify:="test"}
export strEnvVarUserCanModify #help this variable will be accepted if modified by user before calling this script
export strEnvVarUserCanModify2 #help test
strRegexFilter=""
strExample="DefaultValue"
CFGstrTest="Test"
CFGnLayerNumberGap=10
astrRemainingParams=()
astrAllParams=("${@-}") # this may be useful
bUmount=false
bReadOnly=false
bFixDupFiles=false
bPerformSanityChecks=false
bChkIsMultiLayer=false
bChkFoldersReadonly=false
bListActiveLayers=false
bRenumber=false
strIgnoreLayerSuffix=".IGNORE_LAYER"
SECFUNCcfgReadDB #after default variables value setup above
while ! ${1+false} && [[ "${1:0:1}" == "-" ]];do # checks if param is set
	SECFUNCsingleLetterOptionsA;
	if [[ "$1" == "--help" ]];then #help show this help
		SECFUNCshowHelp --colorize "\t[strMountAt]"
		SECFUNCshowHelp --colorize "\tConfig file: '`SECFUNCcfgFileName --get`'"
		echo
		echoc --info "layers ending with '$strIgnoreLayerSuffix' will be skipped, but are still useful for your organization ex. you can create a single layer and put on it symlinks or hardlinks to the ignored layers' files."
		echo
		echoc --info "create layers like (same size for numeric field) ex.:"
		strBasicDirName="BasicDirName"
		echo "$strBasicDirName.layer003.SomeDescription"
		echo "$strBasicDirName.layer010.SomeDescription"
		echo "$strBasicDirName.layer020.Some Description a"
		echo "$strBasicDirName.layer030.Som Description b"
		echo "$strBasicDirName.layer035.Some Description c${strIgnoreLayerSuffix}"
		echo "$strBasicDirName.layer250.Some Desc d"
		echo "$strBasicDirName.layer620.Sm Descrip e"
		echo "..."
		echoc --info "the high value layers will override lower value ones"
		echo
		SECFUNCshowHelp
		exit 0
	elif [[ "$1" == "--fixDupFiles" ]];then #help ~single bFixDupFiles and exit. This will only fix the conflicts, it will not set everything to lowercase (which would actually be simpler...)
		bFixDupFiles=true;
	elif [[ "$1" == "--sanitycheck" ]];then #help ~single perform several sanity checks for the consistency of files and folders, also providing windows compatible results (basically case insensitive non conflicting files and folders). checks for wrong: dup files, dup folders, broken links, writable files and folders on layers
    bPerformSanityChecks=true
	elif [[ "$1" == "-u" ]];then #help ~single unmount and exit
		bUmount=true;
	elif [[ "$1" == "--regex" ]];then #help <strRegexFilter> filter folders ex.: ".*(0010|0039|0014|0500).*"
		shift
		strRegexFilter="$1"
	elif [[ "$1" == "--ro" ]];then #help the mount point will be readonly
		bReadOnly=true;
	elif [[ "$1" == "--is" ]];then #help ~single check if the specified folder is mounted as multilayer mountpoint, and exit status
		bChkIsMultiLayer=true;
	elif [[ "$1" == "--chkro" ]];then #help ~single check if the layers' folders and files are all still readonly
    bChkFoldersReadonly=true
	elif [[ "$1" == "--list" ]];then #help ~single list active (activatable) layers and store in a file for easy backup
    bListActiveLayers=true
	elif [[ "$1" == "--reorder" ]];then #help ~single will find all layers and renumber them with the pre-defined gap <CFGnLayerNumberGap>
		bRenumber=true;
	#~ elif [[ "$1" == "--exampleoption" || "$1" == "-e" ]];then #help <strExample> MISSING DESCRIPTION
		#~ shift
		#~ strExample="${1-}"
	elif [[ "$1" == "--cfg" ]];then #help <strCfgVarVal>... Configure and store a variable at the configuration file with SECFUNCcfgWriteVar, and exit. Use "help" as param to show all vars related info. Usage ex.: CFGstrTest="a b c" CFGnTst=123 help
		shift
		pSECFUNCcfgOptSet "$@";exit 0;
	elif [[ "$1" == "--" ]];then #help params after this are ignored as being these options, and stored at astrRemainingParams
		shift #astrRemainingParams=("$@")
		while ! ${1+false};do	# checks if param is set
			astrRemainingParams+=("$1")
			shift #will consume all remaining params
		done
	else
		echoc -p "invalid option '$1'"
		#"$SECstrScriptSelfName" --help
		$0 --help #$0 considers ./, works best anyway..
		exit 1
	fi
	shift&&:
done
# IMPORTANT validate CFG vars here before writing them all...
SECFUNCcfgAutoWriteAllVars #this will also show all config vars

strMountAt="${1-}"
declare -p strMountAt

if [[ -z "$strMountAt" ]];then
	echoc -p "invalid strMountAt='$strMountAt'"
	exit 1
fi
#strMountAt="`realpath -e Root "$strMountAt"`"
#declare -p strMountAt

SECFUNCuniqueLock --waitbecomedaemon

function FUNCchkFixReadOnlyFolders() {
  echoc --info "Checking if there are folders and files RW"
  local bFoundRW=false
  local lstrRWList="$(find ./ArxLibertatis.layer* -writable -type d)";if [[ -n "$lstrRWList" ]];then bFoundRW=true; echo -e "<<< FOLDERS >>>\n${lstrRWList}";fi
  lstrRWList="$(find ./ArxLibertatis.layer* -writable -type f)";if [[ -n "$lstrRWList" ]];then bFoundRW=true; echo -e "<<< FILES >>>\n${lstrRWList}";fi
  #if (ls -lR "${strMountAt}.layer"* && ls -lR -d "${strMountAt}.layer"*) |egrep "^[d-]rw";then
  if $bFoundRW;then
    echoc --info "@s@gTODO:@S confirm if files are required to be readonly too testing writing on them at main folder"
    echoc -p "all folders, subfolders and files from layers should be readonly or files may be deleted there by mergerfs!"
    if echoc -q "fix them all now (wait at least 2mins to complete fixing)?";then
      date
      #find "${strMountAt}.layer"* -exec chmod -v ugo-w '{}' \; |egrep " changed from .* to " &&:
      SECFUNCexec -ce find "${strMountAt}.layer"* \( -perm -u+w -or -perm -g+w -or -perm -o+w \) -exec chmod -v ugo-w '{}' \;
    fi
  fi
}

function FUNCchkAvail() {
  nMinMB=4200 # seems a requirement for mergerfs TODO find it on it's docs
  while true;do
    nAvailMB=`df -BM --output=avail "$strMountAt" |tail -n 1 |tr -d M`
    if((nAvailMB<nMinMB));then
      echoc --info "Attention! if there isnt at least ${nMinMB}MB available on this media (nAvailMB='$nAvailMB'), you will be unable o write files at '$strMountAt'!"
      echoc --info "And as soon it lowers below that about 100MB, you wont be able to write on it again!"
      if echoc -q -t 60 "Continue anyway?";then
        break
      fi
    else
      break
    fi
  done
}

function FUNCumount(){
  local lastrUmountOpt=(-v)
  local lastrUmountOptFinal=("${lastrUmountOpt[@]}")
  local lastrUmountOptForce=(--force --lazy)
	while ! SECFUNCexecA -ce sudo umount "${lastrUmountOptFinal[@]}" "$strMountAt";do
    SECFUNCexecA -ce lsof |grep "$strMountAt"
    if echoc -q "Failed. If @s@{Rly}you @n!KNOW!@-n what you are doing@S you can let a retry with '${lastrUmountOptForce[@]}' umount?";then
      lastrUmountOptFinal=("${lastrUmountOpt[@]}" "${lastrUmountOptForce[@]}")
    else
      lastrUmountOptFinal=("${lastrUmountOpt[@]}")
    fi
  done
  SECFUNCexecA -ce sudo -k
	SECFUNCexecA -ce trash -v "$strMountAt"
}

pwd
if [[ "${strMountAt:0:1}" == "/" ]];then #absolute path
	strMountedChk="`readlink -e "${strMountAt}"`"&&:
else
	strMountedChk="`readlink -e "$(pwd)"`/${strMountAt}"&&:
fi
declare -p strMountedChk

bAlreadyMounted=false
if mount |grep "on $strMountedChk type fuse.mergerfs";then # MUST BE A NON-REGEX grep!!
	bAlreadyMounted=true;
fi

if $bListActiveLayers;then
  ls -1d "${strMountAt}.layer"* |tee ".${SECstrScriptSelfName}.layers.info"
  exit 0
fi

function FUNCfixDupFilesMain() {
  if ! $bAlreadyMounted;then echoc -p "merged folder must be mounted";exit 1;fi
  
  cd "${strMountAt}"
  local lstrPathMountAt="`pwd`"
  
  local lstrRegexMatchLayers="[.]*/${strMountAt}[.]layer.*"
  
  if echoc -q "do you want to initially just set all files (not folders) to lower case inside all layers? (doing this may speed up the conflicting files fixing later and will completely fix it now. @s@{Yr}Keep work/development files outside layers or they will be lower cased too!@S)";then
    cd "$lstrPathMountAt"
    pwd >&2
    #SECFUNCexec -m "all folders must be RW to let files be fixed" -ce find "../${strMountAt}.layer"* -type d -perm -u-w -exec chmod -v u+w '{}' \;
    function FUNCtoLower() { 
      local lstrFl="$1";
      local lstrPath="`dirname "${lstrFl}"`";
      local lstrBN="$(basename "${lstrFl}")";
      local lstrLower="`echo "$lstrBN"|tr '[:upper:]' '[:lower:]'`";
      local iFixToRWCount=0
      if [[ -f "$lstrPath/$lstrLower" ]];then
        ls -l "$lstrFl" "$lstrPath/$lstrLower"
        echoc -wp "a lower case file already exists!"
      fi
      while ! mv -vT "$lstrFl" "$lstrPath/$lstrLower";do
        if((iFixToRWCount>0));then echoc -w "failed, see above. press a key to retry";fi
        chmod -v u+w "$lstrPath" "$lstrFl"
        ((iFixToRWCount++))&&:
      done
    };export -f FUNCtoLower;
    find ../ -type f -regex "${lstrRegexMatchLayers}/[^/]*[A-Z][^/]*" -exec bash -c "FUNCtoLower '{}'" \;
  fi
  
  function FUNCfixDupFiles() { #all shall be lower case
    local lstrFlOrDir="$1"
    local lstrBN="`basename "$lstrFlOrDir"`"
    local lstrPath="`dirname "$lstrFlOrDir"`"
    declare -p lstrFlOrDir lstrBN lstrPath
    if [[ "$lstrBN" =~ .*[A-Z].* ]];then
      #SECFUNCdrawLine " ${lstrBN} "
      cd "${lstrPath}"
      pwd >&2
      ls -ld --color=always "$lstrBN" >&2
      local lstrBNLC="`echo "$lstrBN" |tr '[:upper:]' '[:lower:]'`"
      while ! SECFUNCexec -ce mv -vT "$lstrBN" "${lstrBNLC}";do
        ls -ld "`pwd`"
        chmod -v u+w "`pwd`"
        echoc -pw "failed, hit a key to retry"
      done
      if ! ls -ld --color=always "$lstrBNLC" >&2;then
        echoc -pw "something went wrong, you should fix the problem before continuing. If the problem is write permission, you can fix that w/o umounting the merged folder."
        exit 1
      fi
    #else
      #echo -en . >&2 #progress hint
    fi
  };export -f FUNCfixDupFiles
  #function FUNCfindAndFixDup() {
    #local lbFolderOnly=false
    #for strDup in "${astrDupList[@]}";do # last folders only
      #if [[ -d "$strDup" ]];then
        #declare -p strDup
        #strBN="`basename "$strDup"`"
        #if [[ "$strBN" =~ .*[A-Z].* ]];then
          #find ../ -iregex ".*/${strMountAt}[.]layer.*/${strDup#./}" -exec bash -c "FUNCfixDupFiles '{}'" \; &&:
        #fi
      #fi
    #done
  #};export -f FUNCfindAndFixDup
  
  echoc --info "searching for DUPs at merged folder"
  cd "$lstrPathMountAt"
  pwd >&2
  #strDups="`find |sort -f |uniq -Ddi`"
  #echo "${strDups}" >&2
  #IFS=$'\n' read -d '' -r -a astrDupList < <(echo "$strDups" |uniq -i)&&:;
  IFS=$'\n' read -d '' -r -a astrDupListTmp < <(find |sort -f |uniq -Ddi)&&:;
  astrDupList=()
  #pwd >&2
  for((i=0;i<${#astrDupListTmp[@]};i++));do #keep only files and folders that need fixing, that have uppercase letters
    local lstrBN="`basename "${astrDupListTmp[i]}"`"
    if [[ "${lstrBN}" =~ .*[A-Z].* ]];then
      echo "adding: [$i]='${astrDupListTmp[$i]}'" >&2
      astrDupList+=("${astrDupListTmp[$i]}")
    fi
    #echo "clearing: [$i]='${astrDupListTmp[$i]}'" >&2
    #unset astrDupList[$i]
  done
  #astrDupList=("${astrDupList[@]}") #clears empties
  #declare -p astrDupList |tr '[' '\n' >&2
  #pwd >&2
  SECFUNCarrayShow -v astrDupList
  #pwd >&2
  IFS=$'\n' read -d '' -r -a astrDupReversedList < <(for strDup in "${astrDupList[@]}";do echo "$strDup";done |sort -r)&&: #the reversed sort list about folders is important so childest can be fixed before parents
  SECFUNCarrayShow -v astrDupReversedList
  
  #pwd >&2
  echoc -w "To fix DUP files and folders, the folders must be writable (the files can remain read only tho). Doing it now." #RWFOLDERS
  #SECFUNCexecA -ce find ../ -type d -iregex "${lstrRegexMatchLayers}" -exec chmod -v u+w '{}' \; #very slow if too many folders
  IFS=$'\n' read -d '' -r -a astrLayersList < <(find ../ -maxdepth 1 -type d -iregex "${lstrRegexMatchLayers}")&&:
  #bRestoreRO=false
  #pwd >&2
  for strLayer in "${astrLayersList[@]}";do
    #pwd
    cd "$strLayer"
    pwd
    for strDup in "${astrDupList[@]}";do 
      if [[ -d "$strDup" ]];then # permissions from parentest folder to childest is the best way to make it work
        chmod -v u+w "$strDup"
      elif [[ -f "$strDup" ]];then
        chmod -v u+w "`dirname "$strDup"`"
      fi
    done
    #pwd
    cd "$lstrPathMountAt"
    #pwd
  done
  
  function FUNCfindFixDup() {
    SECFUNCdrawLine " ${strDup} "
    #declare -p strDup
    #strBN="`basename "$strDup"`"
    #if [[ "$strBN" =~ .*[A-Z].* ]];then
      local lstrEscDup="`echo "${strDup#./}" |sed 's@.@[&]@g'`" #rm ./ and prevents weird chars in filenames messing with regex
      local lstrRegexMatch="${lstrRegexMatchLayers}/${lstrEscDup}"
      declare -p lstrRegexMatch
      find ../ -iregex "${lstrRegexMatch}" -exec bash -c "FUNCfixDupFiles '{}'" \; &&:
    #fi
    return 0
  }
  
  echoc --info "fixing DUP files"
  cd "$lstrPathMountAt"
  for strDup in "${astrDupReversedList[@]}";do # first files only. 
    if [[ ! -d "$strDup" ]];then
      FUNCfindFixDup
    fi
  done
  
  echoc --info "fixing DUP folders"
  cd "$lstrPathMountAt"
  for strDup in "${astrDupReversedList[@]}";do # last folders only. reversed order is important so childest folders are renamed before and wont mess the next iteration
    if [[ -d "$strDup" ]];then
      FUNCfindFixDup
    fi
  done
  
  (cd ..;FUNCchkFixReadOnlyFolders) #fix RWFOLDERS from above
}

if $bFixDupFiles;then
  FUNCfixDupFilesMain
  exit 0
elif $bPerformSanityChecks;then
  if ! SECFUNClookForDupFiles "${strMountAt}";then
    if echoc -q "try to fix dup files and folders?";then
      (FUNCfixDupFilesMain) #$0 --fixDupFiles "${strMountAt}"
      if ! SECFUNClookForDupFiles "${strMountAt}";then
        echoc -p "unable to fix all DUP files and folders"
        exit 1
      fi
    else
      echoc --info "this umount is intended to force merged folder being remounted and re-checked"
      FUNCumount #$0 -u "${strMountAt}"
    fi
  fi
  SECFUNClookForBrokenLinks "${strMountAt}"
  if ! SECFUNClookForBrokenLinks ./;then
    : ${bIgnoreBrokenLinksAtGameFolder:=false} #help
    if ! $bIgnoreBrokenLinksAtGameFolder && ! echoc -q "ignore all broken links this time?";then exit 1;fi
  fi
  FUNCchkFixReadOnlyFolders #$0 --chkro "${strMountAt}"
  exit
elif $bUmount;then
	FUNCumount
	exit 0
elif $bChkIsMultiLayer;then
	if $bAlreadyMounted;then
		exit 0
	fi
	exit 1
elif $bChkFoldersReadonly;then
  FUNCchkFixReadOnlyFolders
  exit 0
fi

if $bAlreadyMounted;then
	echoc --info "already mounted!"
  FUNCchkFixReadOnlyFolders
	exit 0
else
	if [[ -d "$strMountAt" ]];then
		#echoc --info "The merged mount point strMountAt='$strMountAt' will be overriden, and its files will not be accessible."
#		echoc -p "strMountAt='$strMountAt' should not exist..."
		
		#if [[ -z "`ls -A "$strMountAt/"`" ]];then
			#echoc --info "No problem, it is empty."
			#SECFUNCexecA -ce trash -v "$strMountAt"
		#else
		if [[ -n "`ls -A "$strMountAt/"`" ]];then
			echoc -p "The merged mount point strMountAt='$strMountAt' needs to be overriden, and the files existing there will not be accessible: strMountAt='$strMountAt' is not empty!"
			SECFUNCexecA -ce du -sh $strMountAt
			exit 1
		fi
	else
		if [[ -a "$strMountAt" ]];then
			echoc -p "should be a directory strMountAt='$strMountAt', or should not exist..."
			exit 1
		fi
	fi
fi

#declare -A astrLayerList
#IFS=$'\n' read -d '' -r -a astrLayerList < <(find "./" -maxdepth 1 -type d -iname "${strMountAt}.layer*" |sort &&:)&&:
IFS=$'\n' read -d '' -r -a astrLayerList < <(find "./" -maxdepth 1 -type d \( -iname "${strMountAt}.layer*" -and -not -name "*${strIgnoreLayerSuffix}" \) |sort &&:)&&:

astrLayerListBkp=()
if [[ -n "$strRegexFilter" ]];then
	astrLayerListBkp=( "${astrLayerList[@]}" )
	astrLayerList=()
	for strLayer in "${astrLayerListBkp[@]}";do
		if [[ "$strLayer" =~ $strRegexFilter ]];then
			astrLayerList+=("$strLayer")
		fi
	done
fi

declare -p astrLayerList |tr "[" "\n"

#if [[ -z "$strLayerBranch" ]];then
if [[ -z "${astrLayerList[@]-}" ]];then # no layers found
	echoc -p "no layers found"
	exit 1
fi
for strLayer in "${astrLayerList[@]-}";do
	if [[ "$strLayer" =~ .*[:=,].* ]];then
		echoc -p "invalid layer name (must not contain ':' or '=' used by mergerfs, neither ','): $strLayer"
		exit 1
	fi
done
					
if $bRenumber;then
	if $bAlreadyMounted;then
		if echoc -q "umounting required, do it?";then
			FUNCumount
		else
			exit 1
		fi
	fi
	
#	declare -a astrOrderLayerList
	SECbExecJustEcho=true
	for((i=0;i<2;i++));do # 1st time will be just a preview
		iOrder=$CFGnLayerNumberGap
		for strLayer in "${astrLayerList[@]}";do
			strOrder="`echo "${strLayer}" |sed -r "s'(.*${strMountAt}[.]layer)([[:digit:]]*)([.].*)'\2'"`" #collect the numeric order 
	#		astrOrderLayerList[$((10#$strOrder))]="$strOrder:${strLayer}"
			strNewOrder="`printf "%04d" $iOrder`" #$((10#$strOrder))`"
			strNewName="`echo "${strLayer}" |sed -r "s'(.*${strMountAt}[.]layer)([[:digit:]]*)([.].*)'\1${strNewOrder}\3'"`" #modify the numeric order
			if [[ "${strLayer}" != "$strNewName" ]];then
				SECFUNCexecA -cej mv -vT "${strLayer}" "$strNewName"
			else
				echoc --info "skipping unmodified folder name: '${strLayer}'"
			fi
			((iOrder+=CFGnLayerNumberGap))&&:
		done
		
		if ! $SECbExecJustEcho;then break;fi
		if echoc -q "apply it?";then
			SECbExecJustEcho=false # affects the mv command above
		else
			exit 1
		fi
	done
	
	#declare -p astrOrderLayerList
#	iOrder=10
#	for strOrderLayer in "${astrOrderLayerList[@]}";do
#	done
	
	exit 0
fi

# the layers override priority is from left (top override) to right
strLayerBranch=""
astrLayerListInvert=()
for strLayer in "${astrLayerList[@]}";do
	if [[ -n "$strLayerBranch" ]];then strLayerBranch=":$strLayerBranch";fi
	strLayerBranch="${strLayer}${strLayerBranch}"
	astrLayerListInvert=("$strLayer" "${astrLayerListInvert[@]-}")
done
#strLayerBranch="`ls -d "${strMountAt}.layer"* |sort -r |tr "\n" ":" |sed -r 's"(.*):"\1"'`"&&:
#declare -p strLayerBranch astrLayerList astrLayerListInvert |tr ":[" "\n\n"
declare -p astrLayerListInvert |tr "[" "\n"

#iMaxLayers=126 #it is 127 if including the write layer
#echoc --info "total layers = ${#astrLayerList[@]} (iMaxLayers='$iMaxLayers')"
#if((${#astrLayerList[@]}>iMaxLayers));then
	#echoc -p "AUFS layers limit seems to be $iMaxLayers, it may not work..." #TODO confirm also thru documentation?
	#if ! echoc -q "try/continue?";then exit 1;fi
#fi

########
### the leftmost layer will be the one receiving all writes made at the mounted folder, 
### even if it is a write on a file present in a lower layer (such file will remain the same,
### at such lower layer, as long it is modified at the mounted folder)!
########
strWriteLayer="${strMountAt}.0.WriteLayer" #.0 is good to keep on top on filemanagers
SECFUNCexecA -ce mkdir -vp "$strWriteLayer"

astrOpts=()
if $bReadOnly;then
	astrOpts+=(-o ro)
fi

SECFUNCexecA -ce mkdir -vp "$strMountAt"
#declare -p strLayerBranch |tr ":" "\n"
#SECFUNCexecA -ce sudo -k mount -t aufs -o sync,br="$strWriteLayer:$strLayerBranch" ${astrOpts[@]-} none "$strMountAt"
#SECFUNCexecA -m "this first 'none' is just to initialize it, others will be appended" -ce sudo mount -v -t aufs -o "sync,br=$strWriteLayer" ${astrOpts[@]-} none "$strMountAt"
#for strLayer in "${astrLayerListInvert[@]}";do 
	#if [[ -z "$strLayer" ]];then continue;fi #skipper
	#if [[ ! -d "$strLayer" ]];then echoc -p "not a directory?";exit 1;fi 
	
  #echo "appending strLayer='$strLayer'";
  #if ! sudo mount -v -o "remount,append:$strLayer" "$strMountAt";then
    #echoc -p "err=$?";
    #exit 1
  #fi
#done
strBranches="$strWriteLayer"
for strLayer in "${astrLayerListInvert[@]}";do 
	if [[ -z "$strLayer" ]];then continue;fi #skipper
	if [[ ! -d "$strLayer" ]];then echoc -p "not a directory?";exit 1;fi 
	
  echo "appending strLayer='$strLayer'";
  if [[ -n "$strBranches" ]];then strBranches+=":";fi
  #strBranches+="${strLayer#./}"
  strBranches+="${strLayer}"
done

FUNCchkFixReadOnlyFolders

SECFUNCexecA -ce sudo mergerfs -o "defaults,allow_other,use_ino,category.action=ff,category.create=ff,category.search=ff" "$strBranches" "$strMountAt"
SECFUNCexecA -ce sudo -k

strSI="`mount |grep "on $strMountedChk type fuse.mergerfs" |egrep -o "si=[^)]*" |tr "=" "_"`"
declare -p strSI
#SECFUNCexecA -ce ls -l "/sys/fs/aufs/${strSI}/brid"*

#SECFUNCexecA -ce ls -d "${strMountAt}"*
SECFUNCexecA -ce ls -d1 "${astrLayerList[@]}"

#echoc -w "to umount and remove"
#FUNCumount

FUNCchkAvail

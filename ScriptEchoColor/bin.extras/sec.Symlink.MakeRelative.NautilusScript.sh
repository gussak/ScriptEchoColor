#!/bin/bash
# Copyright (C) 2004-2014 by Henrique Abdalla
#
# This file is part of ScriptEchoColor.
#
# ScriptEchoColor simplifies Linux terminal text colorizing, formatting 
# and several steps of script coding.
#
# ScriptEchoColor is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# ScriptEchoColor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ScriptEchoColor. If not, see <http://www.gnu.org/licenses/>.
#
# Homepage: http://scriptechocolor.sourceforge.net/
# Project Homepage: https://sourceforge.net/projects/scriptechocolor/

source <(secinit)

set -x
echo "$PWD"
declare -p PWD
pwd -P
pwd -L
realpath .
set +x
#exit 1
export strPWDWithSymlinks="$PWD"

function FUNCseekRelative() { #helpf <lstrFlLink>
  local lstrFlLink="$1"
  
  declare -p strPWDWithSymlinks >&2
  realpath . >&2
#  IFS=$'\n' read -d '' -r -a astrListPWD < <(realpath . |tr '/' '\n')&&:
  pwd >&2
  #IFS=$'\n' read -d '' -r -a astrListPWD < <(pwd |tr '/' '\n')&&: #using just pwd and not `realpath .`, makes it possible to have relative symlinks pointing to other symlinks!
  declare -p PWD >&2
  #IFS=$'\n' read -d '' -r -a astrListPWD < <(echo "$PWD" |tr '/' '\n')&&: #using just pwd and not `realpath .`, makes it possible to have relative symlinks pointing to other symlinks!
  #IFS=$'\n' read -d '' -r -a astrListPWD < <(pwd -L |tr '/' '\n')&&: #using just pwd and not `realpath .`, makes it possible to have relative symlinks pointing to other symlinks!
  IFS=$'\n' read -d '' -r -a astrListPWD < <(echo "$strPWDWithSymlinks" |tr '/' '\n')&&: #using just pwd and not `realpath .`, makes it possible to have relative symlinks pointing to other symlinks!
  SECFUNCarrayShow --verbose astrListPWD >&2
  #exit 1
  readlink "$lstrFlLink"  >&2
#  IFS=$'\n' read -d '' -r -a astrListLinkPath < <(realpath "`readlink "$lstrFlLink"`" |tr '/' '\n')&&:
  IFS=$'\n' read -d '' -r -a astrListLinkPath < <(readlink "$lstrFlLink" |tr '/' '\n')&&: #using just readlink and not $(realpath "`readlink "$lstrFlLink"`"), makes it possible to have relative symlinks pointing to other symlinks!
  SECFUNCarrayShow --verbose astrListLinkPath >&2
  
  local lstrFixLinkParent="./"
  local lstrFixLinkChild="./"
  local lstrFixLinkChildFromPWD="./"
  local lstrFixLinkChildFromLink="./"
  local liPathIndex=-1
  local liNodeTarget="$(ls -id "$(realpath "`readlink "$lstrFlLink"`")" |awk '{print $1}')"
  local lstrTargetFlBN="$(basename "$(readlink "$lstrFlLink")")"
  #expand --tabs=8
  tabs -8
  #local lbSkipFirstOnly=true
  local lbDiscrepancyBegins=false
  while true;do
    ((liPathIndex++))&&:
    
    function FUNCrep() {
      echo -e "PWD[$liPathIndex]:'${astrListPWD[liPathIndex]-}'\tLn[$liPathIndex]:'${astrListLinkPath[liPathIndex]-}'\tLnNew:'$lstrFixLinkParent/$lstrFixLinkChildFromLink'" >&2
    }
    FUNCrep
    
    if((liPathIndex>=${#astrListPWD[@]} && liPathIndex>=${#astrListLinkPath[@]}));then
      break
    fi
    
    if [[ "${astrListPWD[liPathIndex]-}" == "${astrListLinkPath[liPathIndex]-}" ]];then #ignores matching top parents from root path
      if ! $lbDiscrepancyBegins;then #only while it is still matching parents, not after discrepancy begins
        continue 
      fi
    fi
    
    local lnFlChkInode=$(ls -id "$lstrTargetFlBN" |awk '{print $1}')&&:
    if [[ -f "$lstrTargetFlBN" ]] && [[ -n "$lnFlChkInode" ]] && ((lnFlChkInode==liNodeTarget));then
      echo "!!!FOUND!!!" >&2
    fi
    
    lbDiscrepancyBegins=true
    #lstrFixLinkParent+="../"
    local lbOk=false
    if [[ -n "${astrListPWD[liPathIndex]-}" ]];then 
      lstrFixLinkParent+="../"
      lstrFixLinkChildFromPWD+="/${astrListPWD[liPathIndex]-}" #lstrFixLinkChildFromPWD IS NOT USED
      lbOk=true
    fi
    if [[ -n "${astrListLinkPath[liPathIndex]-}" ]];then
      lstrFixLinkChildFromLink+="/${astrListLinkPath[liPathIndex]-}"
      lbOk=true
    fi
    
    FUNCrep
    
    if ! $lbOk;then break;fi
  done
  #while true;do
    #((liPathIndex++))&&:
    #if [[ -z "${astrListLinkPath[liPathIndex]-}" ]];then break;fi
    #echo -e "$liPathIndex PWD[$liPathIndex]:'${astrListPWD[liPathIndex]-}'\tLn[$liPathIndex]:'${astrListLinkPath[liPathIndex]}'\tLnNew:'$lstrFixLinkParent/$lstrFixLinkChild'" >&2
    
    ## PARENTING
    ##if $lbSkipFirstOnly;then
      ##lbSkipFirstOnly=false;
    ##else
      ##lstrFixLinkParent+="/.."
    ##fi
    
    ## CHILDING
    ##if [[ -z "$lstrFixLinkParent" ]];then
      #if [[ "${astrListPWD[liPathIndex]}" == "${astrListLinkPath[liPathIndex]}" ]];then
        #continue #ignores matching parents
      #fi
    ##fi
    #if [[ -n "${astrListPWD[liPathIndex]-}" ]];then
      #lstrFixLinkParent+="../"
    #fi
    #lstrFixLinkChild+="/${astrListLinkPath[liPathIndex]}"
  #done
  #echo "RESULT: '$lstrFixLinkParent/$lstrFixLinkChild'" >&2
  #echo "$lstrFixLinkParent/$lstrFixLinkChild" #OUTPUT ##################################
  local lstrResult="$lstrFixLinkParent/$lstrFixLinkChildFromLink"
  echo "RESULT: '$lstrResult'" >&2
  echo "$lstrResult" #OUTPUT ##################################
  strFUNCseekRelative_OUTPUT="$lstrResult"
};export -f FUNCseekRelative

#eval astrFiles=(`echo "$NAUTILUS_SCRIPT_SELECTED_FILE_PATHS" |sed 's".*"\"&\""'`)
#IFS=$'\n' read -d '' -r -a astrFiles < <(echo "$NAUTILUS_SCRIPT_SELECTED_FILE_PATHS")
#strFile="${astrFiles[0]}"

#xterm -e "bash -i -c \"echo '$strFile';read\"";exit

strExample="DefaultValue"
bCfgTest=false
bWaitOnExit=false
bRecursive=false
bAlwaysRecreate=false
CFGstrTest="Test"
strParamWithOptionalValue="OptinalValue"
astrRemainingParams=()
export bSkipNautilus=false
export bUsingFileManager=false
if [[ -z "${NAUTILUS_SCRIPT_SELECTED_FILE_PATHS-}" ]];then
	bSkipNautilus=true
else
  bUsingFileManager=true
fi
SECFUNCcfgReadDB #after default variables value setup above
while ! ${1+false} && [[ "${1:0:1}" == "-" ]];do # checks if param is set
	SECFUNCsingleLetterOptionsA;
	if [[ "$1" == "--help" ]];then #help show this help
		SECFUNCshowHelp --colorize "Updates one or more symlinks to its relative target location, as long it is at same or child recursive path."
		SECFUNCshowHelp --colorize "Works at commandline or from nautilus."
		SECFUNCshowHelp
		exit 0
	elif [[ "$1" == "--recreateAlways" ]];then #help always recreate relative symlinks w/o asking
		bAlwaysRecreate=true
	elif [[ "$1" == "--recursive" ]];then #help ~single
		bRecursive=true
	elif [[ "$1" == "--usingfilemanager" || "$1" == "-m" ]];then #help force work like if called by a filemanager. waits on exit
		bUsingFileManager=true
    bWaitOnExit=true
	elif [[ "$1" == "--waitOnExit" || "$1" == "-x" ]];then #help 
		bWaitOnExit=true
	elif [[ "$1" == "--skipnautilus" || "$1" == "-s" ]];then #help even if there is nautilus variables, they will be ignored and command line params will be used
		bSkipNautilus=true
#	elif [[ "$1" == "--examplecfg" || "$1" == "-c" ]];then #help [CFGstrTest]
#		if ! ${2+false} && [[ "${2:0:1}" != "-" ]];then #check if next param is not an option (this would fail for a negative numerical value)
#			shift
#			CFGstrTest="$1"
#		fi
#		
#		bCfgTest=true
	elif [[ "$1" == "--" ]];then #FUNCexample_help params after this are ignored as being these options, and stored at astrRemainingParams
		shift #astrRemainingParams=("$@")
		while ! ${1+false};do	# checks if param is set
			astrRemainingParams+=("$1")
			shift #will consume all remaining params
		done
	else
		echoc -p "invalid option '$1'"
		$0 --help
		exit 1
	fi
	shift&&:
done

iProblemCount=0

if $bRecursive;then
  IFS=$'\n' read -d '' -r -a astrLnkList < <(find -type l)&&:
  for strLnk in "${astrLnkList[@]}";do
    declare -p strLnk
    ls --color=always -l "$strLnk"
  done
  if echoc -q "confirm above?";then
    for strLnk in "${astrLnkList[@]}";do
      echo
      SECFUNCdrawLine " $strLnk "
      SECFUNCexecA -ce ls --color=always -l "$strLnk"
      "$0" --recreateAlways "$strLnk"
    done
  fi
  exit
fi

function FUNCloop() {
	local lbCommandLineByUser=false #TODOA this is confuse. Or is terminal or is thru some filemanager. should just check bUsingFileManager and remove this var.
#	if ! $bSkipNautilus && [[ -n "${NAUTILUS_SCRIPT_SELECTED_FILE_PATHS-}" ]];then
	if ! $bSkipNautilus;then
		IFS=$'\n' read -d '' -r -a astrFiles < <(echo "$NAUTILUS_SCRIPT_SELECTED_FILE_PATHS")
	else
		astrFiles=("$@")
		lbCommandLineByUser=true
	fi
	
	echo "NAUTILUS_SCRIPT_SELECTED_FILE_PATHS='${NAUTILUS_SCRIPT_SELECTED_FILE_PATHS-}'"
	echo "PARAMS: @=`SECFUNCparamsToEval "$@"`"
	
	pwd
	declare -p astrFiles
	
	#local lbFoundProblem=false
	for strFile in "${astrFiles[@]}";do 
		echoc --info "working with strFile='$strFile' ('`realpath -s "$strFile"`', '`ls -ld "$strFile"`')"
    if [[ ! -L "$strFile" ]];then echoc --info "SKIP: not a symlink: strFile='$strFile'";continue;fi
    if [[ ! -a "$strFile" ]];then echoc -p "file not found: strFile='$strFile'";((iProblemCount++))&&:;continue;fi
    
    if $bUsingFileManager;then
      if [[ "$strFile" =~ ^/.* ]];then
        strPWDWithSymlinks="`dirname "$strFile"`" #the filemanager (nemo at least) will use the path it navigated thru symlinks to point to the symlink file to work with!
        #strFile="`basename "$strFile"`"
      fi
    fi
    
    local lstrLnkTgt="`readlink "$strFile"`"
    declare -p lstrLnkTgt
    if ! [[ "`readlink "$strFile"`" =~ ^/.* ]];then lstrLnkTgt="`dirname "$strFile"`/${lstrLnkTgt}";fi
    declare -p lstrLnkTgt
    if [[ ! -a "$lstrLnkTgt" ]];then echoc -p "link target not found: lstrLnkTgt='$lstrLnkTgt'";((iProblemCount++))&&:;continue;fi
    
    if ! [[ "`readlink "$strFile"`" =~ ^/.* ]];then
      local lbAskRecreate=true
      if $bUsingFileManager;then lbAskRecreate=false;fi #using a filemanager will always recreate the symlink
      if $bAlwaysRecreate;then lbAskRecreate=false;fi
      if $lbAskRecreate;then
        if ! echoc -q "strFile='$strFile' is already a relative symlink to , '`readlink "$strFile"`'. Recreate it anyway?";then continue;fi
      fi
    fi
		if ! FUNCmakeRelativeSymlink "$strFile";then
			echoc -p "FUNCmakeRelativeSymlink failed for '$strFile'"
      ((iProblemCount++))&&:;
			#lbFoundProblem=false;
			#break;
		fi
	done
	
  #local lbWait=false
  #if $bUsingFileManager;then lbWait=true;fi
  #if ! $lbCommandLineByUser;then lbWait=true;fi
  #if ! $bWaitOnExit;then lbWait=false;fi #force no wait
  #if ((iProblemCount>0));then lbWait=true;fi #override on problems
  #if $lbWait;then echoc -w -t 60 "iProblemCount=${iProblemCount}";fi
};export -f FUNCloop

function FUNCmakeRelativeSymlink() {
	local lstrFile="${1-}"
	if [[ ! -L "$lstrFile" ]];then
		yad --info --selectable-labels --text "File is not a symlink: '$lstrFile'"
    return 1
  fi

  local lstrFilePath="`dirname "$lstrFile"`"
  if [[ "$lstrFilePath" == "." ]];then #is current path
    lstrFilePath="`pwd`/"
  elif [[ "${lstrFilePath:0:1}" != "/" ]];then #is child of current path
    lstrFilePath="`pwd`/$lstrFilePath"
  fi
  lstrFilePath="`readlink -e "$lstrFilePath"`/" #real canonical path
  echo "lstrFilePath='$lstrFilePath'"
  
  # update file with canonical
  echo "lstrFile='$lstrFile'"
  declare -p lstrFile
  lstrFile="${lstrFilePath}`basename "$lstrFile"`"
  declare -p lstrFile
  
#		echoc --alert "TODO: this functionality is still limited to symlinks pointing to files at the same directory!!! "
  local lstrFullTarget="`readlink -f "$lstrFile"`"
#		if [[ "${lstrFullTarget:0:1}" != "/" ]];then
#			echoc --info "Skipping: already relative symlink '$lstrFile' points to '$lstrFullTarget'."
#			return 0
#		fi

#		local lstrDirname="`dirname "$lstrFullTarget"`" #the path can be symlinked, the lstrFullTarget will work ok when matching for lstrDirname removal
#		echo "lstrDirname='$lstrDirname'"
  
#		local lstrNewSymlinkTarget="`basename "$lstrFullTarget"`"
#		local lstrNewSymlinkTarget="./${lstrFullTarget#$lstrDirname}"
#		local lstrNewSymlinkTarget="${lstrFullTarget#$lstrDirname}"
  local lstrNewSymlinkTarget="${lstrFullTarget#$lstrFilePath}"
  echo "lstrNewSymlinkTarget='$lstrNewSymlinkTarget'"
  while [[ "${lstrNewSymlinkTarget:0:1}" == "/" ]];do
    lstrNewSymlinkTarget="${lstrNewSymlinkTarget:1}"
    echo "lstrNewSymlinkTarget='$lstrNewSymlinkTarget'"
  done
  declare -p lstrNewSymlinkTarget lstrFullTarget lstrFilePath
  
  ( # work on working file path
    SECFUNCexecA -ce cd "$lstrFilePath"
#		if [[ -a "`dirname "$lstrFile"`/$lstrNewSymlinkTarget" ]];then
#		if [[ -a "$lstrDirname/$lstrNewSymlinkTarget" ]];then
    function FUNCretErr() {
      local lnLineNo="$1";shift
      echoc -pw "failed LINE=$lnLineNo '${@-}', exit"
      return 1
    }
    if [[ -a "$lstrNewSymlinkTarget" ]];then
      #echoc -x "rm -v '$lstrFile'"
      if ! SECFUNCexecA -ce ln -vsfT "$lstrNewSymlinkTarget" "$lstrFile";then
        FUNCretErr $LINENO
      fi
    else
      function FUNCfixManually() {
        echoc -x pwd
        SECFUNCexecA -ce ls --color=always -l "$lstrFile"
        local lstrTargetManual=""
        if which yad;then
          lstrTargetManual="`SECFUNCexec -ce yad --selectable-labels --title "$0" --text "failed, sorry... fix the relative symlink manually please.\n  PWD=$(pwd)\n  lstrFile=$lstrFile\n  REALPATH=$(realpath "$lstrFile")\n  SymLink=$(readlink "$lstrFile")" --entry --entry-text "$(readlink "$lstrFile")"`";
        else
          lstrTargetManual="`echoc -S "Set it manually:@D$(readlink "$lstrFile")"`"
        fi
        if [[ -f "$lstrTargetManual" ]];then
          SECFUNCexecA -ce ln -vsfT "$lstrTargetManual" "$lstrFile";
          SECFUNCexecA -ce ls --color=always -l "$lstrTargetManual" "$lstrFile"
          return 0
        else
          FUNCretErr $LINENO
        fi
      }
      #FUNCseekRelative "$lstrFile"
      #local lstrSeekRelative="$(FUNCseekRelative "$lstrFile")"&&:;local lRetSeekR=$? #TODO why this may give totally weird results!?!?
      #for((i=0;i<${#lstrSeekRelative};i++));do echo -n "[$i]${lstrSeekRelative:$i:1}";done #TODO what is going on !?!??!?!
      #echo "${#lstrSeekRelative}"
      #declare -p lstrSeekRelative
      local lstrSeekRelativeWeirdOrNot="$(FUNCseekRelative "$lstrFile")"&&:;local lRetSeekR=$?
      declare -p lstrSeekRelativeWeirdOrNot
      FUNCseekRelative "$lstrFile"&&:;local lRetSeekR=$?
      lstrSeekRelative="$strFUNCseekRelative_OUTPUT"
      declare -p lstrSeekRelative
      if [[ "$lstrSeekRelativeWeirdOrNot" != "$strFUNCseekRelative_OUTPUT" ]];then
        echo "#lstrSeekRelativeWeirdOrNot=${#lstrSeekRelativeWeirdOrNot}"
        echo "#strFUNCseekRelative_OUTPUT=${#strFUNCseekRelative_OUTPUT}"
        echo "lstrSeekRelativeWeirdOrNot:"
        for((i=0;i<${#lstrSeekRelativeWeirdOrNot};i++));do echo -n "[$i]${lstrSeekRelative:$i:1}";done #TODO what is going on !?!??!?!
        echo
      fi
      #echo -e "AB\nCDlstrSeekRelative='$lstrSeekRelative'\nED$lstrSeekRelative" >&2
      if ((lRetSeekR==0));then # && [[ -f "$lstrSeekRelative" ]];then
        declare -p lstrFile&&:
        SECFUNCexecA -ce ls --color=always -l "$lstrSeekRelative" "$lstrFile"&&:
        pwd
        local lbSuccess=true
        if $lbSuccess && [[ ! -a "${lstrSeekRelative}" ]];then lnLineErr=$LINENO;lbSuccess=false;fi
        if $lbSuccess && [[ ! -a "${lstrFile}" ]];then lnLineErr=$LINENO;lbSuccess=false;fi
        if $lbSuccess && [[ ! -a "`readlink "$lstrFile"`" ]];then lnLineErr=$LINENO;lbSuccess=false;fi
        iNodeSeekRel=$(ls -id "${lstrSeekRelative}" |awk '{print $1}')&&:
        iNodeTarget=$(ls -id "`readlink "$lstrFile"`" |awk '{print $1}')&&:
        declare -p iNodeSeekRel iNodeTarget
        if $lbSuccess && ((iNodeSeekRel!=iNodeTarget));then lnLineErr=$LINENO;lbSuccess=false;fi
        #if cmp "${lstrSeekRelative}" "$lstrFile";then
        if $lbSuccess;then
          declare -p lstrSeekRelative #cleanups
          lstrSeekRelative="$(echo "$lstrSeekRelative" |sed -r -e 's@^[.]/@@')"
          declare -p lstrSeekRelative
          lstrSeekRelative="$(echo "$lstrSeekRelative" |sed -r -e 's@//@/@g')"
          declare -p lstrSeekRelative
          lstrSeekRelative="$(echo "$lstrSeekRelative" |sed -r -e 's@/[.]/@/@g')"
          declare -p lstrSeekRelative
          if SECFUNCexecA -ce ln -vsfT "$lstrSeekRelative" "$lstrFile";then
            ls --color=always -l "$lstrFile"
            return 0
          else
            if FUNCfixManually;then
              return 0;
            fi
            FUNCretErr $LINENO
          fi
        else
          echoc -p "failed at lnLineErr='$lnLineErr'"
          if FUNCfixManually;then
            return 0;
          fi
          FUNCretErr $LINENO
        fi
      else
        echoc -p "unable to make relative symlink '$lstrFile' to '$lstrNewSymlinkTarget'"
        if FUNCfixManually;then
          return 0;
        fi
        echoc -w "fail, exit"
        FUNCretErr $LINENO
      fi
    fi
  )
  
  return 0
};export -f FUNCmakeRelativeSymlink

#if ! $bSkipNautilus && [[ -n "${NAUTILUS_SCRIPT_SELECTED_FILE_PATHS-}" ]];then
if ! $bSkipNautilus;then
	#cd "/tmp" #NAUTILUS_SCRIPT_SELECTED_FILE_PATHS has absolute path to selected file
	#xterm -e "bash -i -c \"FUNCloop\"" # -i required to force it work
	secXtermDetached.sh --ontop --title "`SECFUNCfixId --justfix -- "${SECstrScriptSelfName}"`" --skiporganize FUNCloop "$@"
	#for strFile in "${astrFiles[@]}";do 
	#	if ! xterm -e "bash -i -c \"FUNCmakeRelativeSymlink '$strFile'\"";then # -i required to force it work on ubuntu 12.10
	#		break;
	#	fi
	#done
else
	SECFUNCexecA -ce FUNCloop "$@" #user is using commandline 
fi

declare -p bWaitOnExit
lbWait=$bWaitOnExit
nWait=60
if $bUsingFileManager;then lbWait=true;fi
#if ! $lbCommandLineByUser;then lbWait=true;fi
#if ! $bWaitOnExit;then lbWait=false;fi #force no wait
if ((iProblemCount>0));then  #override on problems
  echoc -p "iProblemCount=${iProblemCount}"
  lbWait=true;
  nWait=600
fi 
if $lbWait;then 
  echoc -w -t $nWait
fi

#!/bin/bash
# Copyright (C) 2023 by Henrique Abdalla
#
# This file is part of ScriptEchoColor.
#
# ScriptEchoColor simplifies Linux terminal text colorizing, formatting 
# and several steps of script coding.
#
# ScriptEchoColor is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# ScriptEchoColor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ScriptEchoColor. If not, see <http://www.gnu.org/licenses/>.
#
# Homepage: http://scriptechocolor.sourceforge.net/
# Project Homepage: https://sourceforge.net/projects/scriptechocolor/

source <(secinit)

strFlBase="/tmp/`basename "$0"`."

if [[ "${1-}" == "--help" ]];then
	SECFUNCshowHelp #egrep "[#]help" "$0"
	exit 0
fi

if [[ "${1-}" == "--getExecFileForUID" ]];then
  shift
  echo "Showing id for current term on guake:" >&2
  echo "${strFlBase}$1"
  exit 0
fi

#TODO for yakuake too
if [[ -z "${GUAKE_TAB_UUID-}" ]];then declare -p GUAKE_TAB_UUID;exit 0;fi # this means it is not in guake

: ${bBashAutoCmdOnStart:=true} #help
if ! $bBashAutoCmdOnStart;then ps >&2;pwd >&2;declare -p bBashAutoCmdOnStart >&2;exit 0;fi

ps >&2;pwd >&2
strFlExec="${strFlBase}$GUAKE_TAB_UUID"
nCount=0
: ${fFastCheckCountDown:=90} #help in seconds. begins fast, so when sending multiple commands that may create child bash, that will be faster
: ${fFastDelay=0.33} #help
: ${fSlowDelay=3} #help
: ${fFastCheckCoolDown:=10} #help every time a command is executed, will wait this much in fast mode
#nCountWaiting=0
#bFast=true #begins fast, so when sending multiple commands that may create child bash, that will be faster
while true;do
  echo -ne "$SECONDS(FastTmOut:$fFastCheckCountDown): wating terminal auto cmd ($strFlExec). Hit any key for prompt.\r"  >&2;
  #TODO test if terminal is interactive, I think `read` is failing when guake loads initially on pc restart
  #fSleep=3;if $bFast;then fSleep=0.33;fi
  if $(SECFUNCbcPrettyCalc --cmp "$fFastCheckCountDown > 0");then
		fFastCheckCountDown=$(SECFUNCbcPrettyCalc "$fFastCheckCountDown - $fFastDelay")
		fSleep=$fFastDelay
  else
		fFastCheckCountDown=0
		fSleep=$fSlowDelay
  fi
  
  if read -t $fSleep -n 1;then break;fi;
  
  if [[ -f "$strFlExec" ]];then
    echoc --info "EXECUTING file contents '$strFlExec' here:" >&2
    cat "$strFlExec" >&2
    strRunFl="${strFlExec}.Running.${nCount}"
    mv -v "$strFlExec" "$strRunFl" >&2
    #chmod +x "${strFlExec}.Running"
    eval "export strBashAutoCmdFl${nCount}='$strRunFl'"
    SECFUNCexecA -ce source "${strRunFl}"&&:
    ps >&2;pwd >&2
    #bash -c "${strFlExec}.Running;ps;pwd;bash"
		if $(SECFUNCbcPrettyCalc --cmp "$fFastCheckCountDown < $fFastCheckCoolDown");then
			fFastCheckCountDown=$(SECFUNCbcPrettyCalc $fFastCheckCountDown+$fFastCheckCoolDown)
		fi
    ((nCount++))&&:
  fi
done
declare -p PATH >&2;ps >&2;pwd >&2;
bBashAutoCmdOnStart=false bash # this prevent this self script recurrency

#!/bin/bash
# Copyright (C) 2021 by Henrique Abdalla
#
# This file is part of ScriptEchoColor.
#
# ScriptEchoColor simplifies Linux terminal text colorizing, formatting 
# and several steps of script coding.
#
# ScriptEchoColor is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# ScriptEchoColor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ScriptEchoColor. If not, see <http://www.gnu.org/licenses/>.
#
# Homepage: http://scriptechocolor.sourceforge.net/
# Project Homepage: https://sourceforge.net/projects/scriptechocolor/

source <(secinit)

strVarEvalBaseId="`SECFUNCfixIdA -f "${SECstrScriptSelfName}"`"

: ${nSleep:=2} #help
: ${bMoveAwayFromEdge:=false} #help

#help edge detection works even if the screen is locked.
#help edge will only trigger once when mouse is on it, have to leave the edge before trying to trigger it again.
#help important: all top edges are very used by the systray, and the BottomMiddle may be used by cairo-dock, so these should not be used.
bOnce=true;if [[ "${1-}" == --daemon ]]; then bOnce=false;shift&&:;fi; #help daemon loop
bNum=false; if [[ "${1-}" == --numeric ]];then bNum=true; shift&&:;fi; #help numeric edge/corner index instead of IDname
bInfo=false;if [[ "${1-}" == --info ]];   then bInfo=true;shift&&:;fi; #help just output info about edges and exit
#bEchoOnlyOnClick=false;if [[ "${1-}" == --clicks ]]; then bEchoOnlyOnClick=true;shift&&:;fi; #help will only echo a result if a 'click' happens (when leaving the edge)
bClearBuffer=false;if [[ "${1-}" == --clear ]]; then bClearBuffer=true;shift&&:;fi; #help will clear the buffered events once
bFIFO=false;if [[ "${1-}" == --consume ]]; then bFIFO=true;shift&&:;fi; #help will consume and output the oldest event in the buffer, it can be `eval`
if [[ "${1-}" == --help ]];then egrep "[#]help" $0 >/dev/stderr;exit 0;fi

if ! $bOnce;then
  if SECFUNCuniqueLock --isdaemonrunning;then echo "daemon already running." >/dev/stderr;exit 0;fi
  SECFUNCuniqueLock --waitbecomedaemon
fi

read nScreenWitdh nScreenHeight < <(xrandr |grep "primary" |sed -r 's".*primary ([[:digit:]]*)x([[:digit:]]*).*"\1 \2"'); 
#declare -p nScreenWitdh nScreenHeight >/dev/stderr
#anConvertEdgeToIndex[0]=0
#anConvertEdgeToIndex[1]=1
#anConvertEdgeToIndex[2]=2
#anConvertEdgeToIndex[3]=0
#anConvertEdgeToIndex[4]=0
#anConvertEdgeToIndex[5]=0
#anConvertEdgeToIndex[6]=0
#anConvertEdgeToIndex[7]=0
#anConvertEdgeToIndex=(
  #0  1 2
  #3 -1 4
  #5  6 7
#)
astrEdge=(
  TL TM TR
  ML    MR
  BL BM BR
)
astrEdgeLong=(
  TopLeft    TopMiddle    TopRight
  MiddleLeft              MiddleRight
  BottomLeft BottomMiddle BottomRight
)
if $bInfo;then
  for((i=0;i<3;i++));do
    for((i2=0;i2<3;i2++));do
      echo -n "${astrEdge[$(( (i*3)+i2 ))]} "
    done
    echo
  done
  for((i=0;i<3;i++));do
    for((i2=0;i2<3;i2++));do
      echo -n "${astrEdgeLong[$(( (i*3)+i2 ))]} "
    done
    echo
  done
  declare -p astrEdge astrEdgeLong
  exit 0
fi

function FUNCechoEdge() { #<nEdge>
  local lnEdge="$1"
  
  if $bNum;then
    echo $lnEdge
  else
    if((lnEdge==-1));then
      echo NO #none
    else
      echo "${astrEdge[$lnEdge]}"
    fi
  fi
}

CFGanBuffer=()
SECFUNCcfgReadDB

if $bFIFO;then
  # reset them to the external script become consistent with current edge status
  echo "\
    ${strVarEvalBaseId}_nEdge=-1;\
    ${strVarEvalBaseId}_strEdge=\"NO\";\
    ${strVarEvalBaseId}_strEdgeLong=\"None\";\
    ${strVarEvalBaseId}_nTime=-1;\
    "

  if((`SECFUNCarraySize CFGanBuffer` > 0));then
    echo "${CFGanBuffer[0]}"
    CFGanBuffer=("${CFGanBuffer[@]:1}") # remove first item
    SECFUNCcfgAutoWriteAllVars
  fi
  exit 0
fi

if $bClearBuffer;then
  declare -p CFGanBuffer
  CFGanBuffer=()
  SECFUNCcfgAutoWriteAllVars
  declare -p CFGanBuffer
  exit 0
fi

nEdgePrevious=-1
nEdge=-1
# 0 1 2
# 3   4
# 5 6 7
: ${nMargin:=100} #help base margin
nMarginP25=$((nMargin*25/100)) #25%
while true;do 
  # first things
  SECFUNCcfgReadDB
  
  # middle things
  
  read nMouseX nMouseY < <(xdotool getmouselocation --shell|egrep "^(X|Y)" |tr -d "XY=" |tr "\n" " "&&:)&&:
  declare -p nMouseX nMouseY >/dev/stderr;
  
  nEdge=-1
  # corners
  if((nMouseX<=nMarginP25 && nMouseY<=nMarginP25 ));then nEdge=0;fi #TL
  if((nMouseX<=nMarginP25 && nMouseY>=(nScreenHeight-nMarginP25) ));then nEdge=5;fi #BL
  if((nMouseX>=(nScreenWitdh-nMarginP25) && nMouseY<=nMarginP25 ));then nEdge=2;fi #TR
  if((nMouseX>=(nScreenWitdh-nMarginP25) && nMouseY>=(nScreenHeight-nMarginP25) ));then nEdge=7;fi #BR
  # middles
  if((nMouseX>=(nScreenWitdh /2-nMargin) && nMouseX<=(nScreenWitdh /2+nMargin) && nMouseY<=nMarginP25 ));then nEdge=1;fi
  if((nMouseX>=(nScreenWitdh /2-nMargin) && nMouseX<=(nScreenWitdh /2+nMargin) && nMouseY>=(nScreenHeight-nMarginP25) ));then nEdge=6;fi
  if((nMouseY>=(nScreenHeight/2-nMargin) && nMouseY<=(nScreenHeight/2+nMargin) && nMouseX<=nMarginP25 ));then nEdge=3;fi
  if((nMouseY>=(nScreenHeight/2-nMargin) && nMouseY<=(nScreenHeight/2+nMargin) && nMouseX>=(nScreenWitdh-nMarginP25) ));then nEdge=4;fi
  
  if $bMoveAwayFromEdge && ((nEdge>-1));then
    # the problem is double move the mouse trying to make it reach the edge
    # other problem is an auto move away happens while we are still moving the mouse
    nMvX=0;nMvY=0;nMv=$(($nMarginP25+5))
    case $nEdge in
      0)nMvX=$nMv;nMvY=$nMv;;
      1)nMvY=$nMv;;
      2)nMvX=-$nMv;nMvY=$nMv;;
      
      3)nMvX=$nMv;;
      4)nMvX=-$nMv;;
      
      5)nMvX=$nMv;nMvY=-$nMv;;
      6)nMvY=-$nMv;;
      7)nMvX=-$nMv;nMvY=-$nMv;;
    esac
    xdotool mousemove_relative -- $nMvX $nMvY
  fi
  
  #bClick=false
  #if [[ "$nEdge" != "${nEdgePrevious-}" ]];then
    #if ((nEdgePrevious>-1));then
      #bClick=true
    #fi
  #fi
  
  #bEcho=false
  #if $bEchoOnlyOnClick;then
    #if $bClick;then
      #bEcho=true
    #fi
  #else
    #bEcho=true
  #fi

  if((nEdge>-1)) && ((nEdge != nEdgePrevious));then
    nTime=`date +%s`
  
    CFGanBuffer+=("\
      ${strVarEvalBaseId}_nEdge=$nEdge;\
      ${strVarEvalBaseId}_strEdge=\"${astrEdge[$nEdge]}\";\
      ${strVarEvalBaseId}_strEdgeLong=\"${astrEdgeLong[$nEdge]}\";\
      ${strVarEvalBaseId}_nTime=$nTime;\
      ")
    
    FUNCechoEdge $nEdge
    
    secNotify.sh "${SECstrScriptSelfName}_Edge" "${astrEdgeLong[$nEdge]} (${SECstrScriptSelfName})"
  fi
  if((nEdge==-1));then
    secNotify.sh "${SECstrScriptSelfName}_Edge" ""
  fi
  
  nEdgePrevious="$nEdge"
  
  # last things
  SECFUNCcfgAutoWriteAllVars
  SECFUNCarrayShow CFGanBuffer
  
  if $bOnce;then exit 0;fi
  sleep $nSleep;
done

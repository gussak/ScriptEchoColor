#!/bin/bash
# Copyright (C) 2020-2020 by Henrique Abdalla
#
# This file is part of ScriptEchoColor.
#
# ScriptEchoColor simplifies Linux terminal text colorizing, formatting 
# and several steps of script coding.
#
# ScriptEchoColor is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# ScriptEchoColor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ScriptEchoColor. If not, see <http://www.gnu.org/licenses/>.
#
# Homepage: http://scriptechocolor.sourceforge.net/
# Project Homepage: https://sourceforge.net/projects/scriptechocolor/

source <(secinit)

egrep "[#]help" $0

declare -A CFGanNotifIdList=()
function FUNCnotifyCmd() {
	local lstrKey="$1";shift
	local lstrTitle="$1";shift
	local lstrContent="${1-}";shift
	
	local lstrMyAppName="$lstrTitle"
	gdbus call \
		--session \
		--dest org.freedesktop.Notifications \
		--object-path /org/freedesktop/Notifications \
		--method org.freedesktop.Notifications.Notify \
		"$lstrMyAppName" 0 dummy "$lstrTitle" "$lstrContent" "[]" "{}" 0
	return 0
}
function FUNCnotify() { # <lstrKey>
	local lstrKey="$1";shift
	local lstrTitle="$1";shift # shows on lock screen
	local lstrContent="${1-}";shift&&: # description/comments, wont show on lock screen
	
	CFGanNotifIdList[$lstrKey]="$(FUNCnotifyCmd "$lstrKey" "$lstrTitle" "$lstrContent" |awk '{print $2}' |tr -d ",)" )";
  #echo "DEBUG: '${CFGanNotifIdList[$lstrKey]}'"
	
	return 0
}
function FUNCnotifyDelLast() { #<lnNotifID>
  local lstrKey="$1"
  
	local lnNotifID="${CFGanNotifIdList[$lstrKey]-}"
	if [[ -n "$lnNotifID" ]];then
		gdbus call \
			--session \
			--dest org.freedesktop.Notifications \
			--object-path /org/freedesktop/Notifications \
			--method org.freedesktop.Notifications.CloseNotification \
			$lnNotifID	>/dev/null
      
    CFGanNotifIdList[$lstrKey]=""
	fi
	return 0
}

# params
strKey="$1";shift #help REQUIRED unique key for this notification, use the script name as base for this ID
strTitle="$1";shift #help OPTIONAL the title is visible on lock screen, if empty will just delete the previous notification for the ID
strContent="${1-}";shift&&: #help OPTIONAL description/comments, wont show on lock screen

# main work
SECFUNCcfgReadDB

#FUNCnotifyDelLast "${CFGanNotifIdList[$strKey]-}"
FUNCnotifyDelLast "$strKey"
if [[ -n "${strTitle}" ]];then
  FUNCnotify "$strKey" "${strTitle}" "$strContent"
fi

SECFUNCcfgAutoWriteAllVars -n

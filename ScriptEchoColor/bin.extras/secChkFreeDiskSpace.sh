#!/bin/bash
# Copyright (C) 2023 by Henrique Abdalla
#
# This file is part of ScriptEchoColor.
#
# ScriptEchoColor simplifies Linux terminal text colorizing, formatting 
# and several steps of script coding.
#
# ScriptEchoColor is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# ScriptEchoColor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ScriptEchoColor. If not, see <http://www.gnu.org/licenses/>.
#
# Homepage: http://scriptechocolor.sourceforge.net/
# Project Homepage: https://sourceforge.net/projects/scriptechocolor/

source <(secinit)

astrIgnore=("/run/lock")

while true;do
  
  IFS=$'\n' read -d '' -r -a astrFsList < <(df --output=target |tail -n +2)&&:
  
  strAlert=""
  aYadColumns=()
  nFsCount=0
  strLastFs=""
  for strFs in "${astrFsList[@]}";do 
    bSkip=false
    for strIgn in "${astrIgnore[@]}";do 
      if SECFUNCarrayContains astrIgnore "$strFs";then
        bSkip=true
        break;
      fi
    done
    if $bSkip;then continue;fi
    
    nAvailKB="$(df -BK --output=avail "$strFs" |tr -d ' K' |tail -n +2)";
    #declare -p nAvailKB strFs;
    if(($nAvailKB<100000));then 
      #secNotify.sh DiskSpaceAlmostNotAvailable "$strFs=$nAvailKB";
      strAlert+="!!! $((nAvailKB/1024))MB !!! @ $strFs \n"
      aYadColumns+=(false $((nAvailKB/1024)) "$strFs")
      ((nFsCount++))&&:
      strLastFs="$strFs"
    fi;
  done
  
  if [[ -n "$strLastFs" ]];then
    strAlert+="\n"
    strAlert+="(strLastFs='$strLastFs') \n"
  fi
  
  if [[ -n "$strAlert" ]];then
    mkdir -vp ~/.themes/temp/gtk-3.0;echo "box{background-image:image(rgb(255,0,0));}" >~/.themes/temp/gtk-3.0/gtk.css;
    GTK_THEME="temp" yad \
      --center \
      --on-top \
      --timeout 15 \
      --title "almost no free disc space available!" \
      --text "$strAlert" \
      --button="(0)Ignore10Minutes" \
      --button="(1)IgnoreThisSession"&&:;nRet=$? # nRet=70 means it timedout
    if((nRet==0));then 
      sleep 600
    fi
    if((nRet==1));then
      if((nFsCount==1));then
        astrIgnore+=("$strLastFs")
      else
        IFS=$'\n' read -d '' -r -a astrList < <(yad \
          --maximized \
          --center \
          --no-markup \
          --list \
          --checklist \
          --column="AddToIgnore" --column "FreeMB" --column "Filesystem" \
          "${aYadColumns[@]}" \
          &&:)&&:;nRet=$?
        for strLine in "${astrList[@]}";do
          strFsIgn="`echo "$strLine" |cut -d'|' -f 3`"
          astrIgnore+=("$strFsIgn")
        done
      fi
      declare -p astrIgnore
    fi
  else
    sleep 60
  fi
done

#strTitle="${1-}"
#strFileDrive="${2-}"
#nWarnLimit="${3-}"
#while true;do
	#nAvail="`df -B1 --output=avail "$strFileDrive" |tail -n 1`"
	#if((nAvail<nWarnLimit));then
		#echoc --say "out of disk space at $strTitle"
	#fi
	#echoc --info "$strTitle, $strFileDrive, nAvail=$nAvail, nWarnLimit=$nWarnLimit"
	#echoc -w -t 60
#done


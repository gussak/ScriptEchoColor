#!/bin/bash
# Copyright (C) 2023 by Henrique Abdalla
#
# This file is part of ScriptEchoColor.
#
# ScriptEchoColor simplifies Linux terminal text colorizing, formatting 
# and several steps of script coding.
#
# ScriptEchoColor is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# ScriptEchoColor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ScriptEchoColor. If not, see <http://www.gnu.org/licenses/>.
#
# Homepage: http://scriptechocolor.sourceforge.net/
# Project Homepage: https://sourceforge.net/projects/scriptechocolor/

source <(secinit)

nWId="`wmctrl -l |grep "Guake!" |awk '{print $1}'`";declare -p nWId

: ${bUseBashAutoCmdMode:=false}
if ! qdbus org.guake3.RemoteControl /org/guake3/RemoteControl |grep execute_command_by_uuid;then
  #echoc --alert "@{-n}execute_command_by_uuid was @nremoved@{-n}, therefore this script wont work that way anymore :(";
  bUseBashAutoCmdMode=true
fi

if [[ -z "${1-}" ]] || [[ "${1}" == --help ]];then
  #help Usage ex.: [-x] -ID_CMD coolName ls 123 -ID_CMD funnyName ls 456 789
  egrep "[#]help" $0 |sed -r -e 's"^[[:space:]]*""' -e 's"[[:space:]]*$""' >/dev/stderr
  exit 0
fi

export bUsingXtermToRun=false;
if [[ "${1-}" == "-x" ]];then 
  bUsingXtermToRun=true;
  shift&&:;
  (xterm -e $0 "$@"&disown) #w/o -x now
  exit
fi

set -Eeu

# depends on
while ! pgrep -fa "`which guake`";do echo " >>>>> WARN:WaitingGuakeStart";sleep 3;done #this line checks for a running guake
while ! qdbus org.guake3.RemoteControl;do echo " >>>>> WARN:WaitingGuakeDBus";sleep 3;done #this line checks for a listening guake

function FUNCexecEcho() {
  echo " EXEC: $@" >/dev/stderr
  "$@"
};export -f FUNCexecEcho

function FUNCgrc() {
  FUNCexecEcho qdbus org.guake3.RemoteControl /org/guake3/RemoteControl org.guake3.RemoteControl."$@"
}

# read parameters
acmd=()
astrIdList=()
astrOptList=()
iIdIndex=0
function FUNCchkCmd() { if [[ "${1:0:7}" == "-ID_CMD" ]];then return 0;else return 1;fi; } # <"$1">
while ! ${1+false};do
  if FUNCchkCmd "${1}";then
    echo " >>> NEW ID_CMD iIdIndex=$iIdIndex:"
    astrOptList+=("${1#-ID_CMD}");shift
    astrIdList+=("$1");shift;
    continue;
  fi
  
  acmd+=("$1");shift
  if ${1+false} || FUNCchkCmd "${1}";then
    echo "astrOptList[$iIdIndex]='${astrOptList[$iIdIndex]}'"
    echo "astrIdList[$iIdIndex]='${astrIdList[$iIdIndex]}'"
    eval 'aIdCmd'${iIdIndex}'=("${acmd[@]}")';declare -p aIdCmd${iIdIndex}
    ((iIdIndex++))&&:
    acmd=()
  fi
done
declare -p astrIdList

# detect existing tabs
iTabCount="`FUNCgrc get_tab_count`"
iCurrentTabIndex="`FUNCgrc get_selected_tab`"
strCurrentTabUUID="`FUNCgrc get_selected_uuidtab`"
astrTabUUIDList=()
astrTabNameList=()
for((i=0;i<iTabCount;i++));do
  echo
  declare -p i
  strTabName="`FUNCgrc get_tab_name $i`";declare -p strTabName
  if((i==iCurrentTabIndex));then echo " CURRENT TAB";fi
  
  FUNCgrc select_tab $i
  astrTabUUIDList+=("`FUNCgrc get_selected_uuidtab`")
  astrTabNameList+=("`FUNCgrc get_tab_name $i`")
done
FUNCgrc select_tab $iCurrentTabIndex # this first is reached fast so the user has not to wait a long time
: ${bGoBackToInitialTab:=false} #help will re-select select the tab this script was run on at the end

declare -p astrTabUUIDList |tr '[' '\n'
declare -p astrTabNameList |tr '[' '\n'

function FUNCrunCmd() {
  strOpts="$1";shift
  strTabName="$1";shift
  strTabUUID="$1";shift
  iIdCmd="$1";shift

  aOptList=();if [[ -n "$strOpts" ]];then aOptList+=(`echo "$strOpts" |sed -r 's@(.)@\1 @g'`);fi
  
  echo
  echo " TabName: ${strTabName}, CMDOPTION(s): ${aOptList[@]} "
  
  eval 'acmdRun=("${aIdCmd'${iIdCmd}'[@]}")';declare -p acmdRun
  
  bSubShell=true
  bForceCurrentTab=false
  bRetryOnError=false
  for strOpt in "${aOptList[@]}";do
    case "$strOpt" in
      n)bSubShell=false;; #help CMDOPTION: The default is to run using `bash -c '$cmd'`. To disable this, use like ex.: `-ID_CMDn cd ComePathToApplyAtCurrentShell`
      c)bForceCurrentTab=true;if ! $bUsingXtermToRun;then echo " >>>>> WARNING <<<<< 'c' CMDOPTION is more reliable using '-x' option!!!";fi;; #help CMDOPTION: The tab name will only be ignored (but only if it does not exist ex.: "IgnoredCurrentTabName") and the current tab running this script will be used. This option is more reliable with -x option.
      r)bRetryOnError=true;; #help CMDOPTION: if the command fails returning any error, it will retry running it after 3s.
      *)$0 --help;echo "ERROR: invalid option '$strOpt' for strTabName='${strTabName}' and command: ${acmdRun[*]}";exit 1;;
    esac
  done
  
  # create and run a tmp script
  strFl="$(mktemp)";chmod +x "$strFl"
  echo '#!/bin/bash
    cat $0; # review the command
    '"$(declare -p acmdRun)"'; # '"${acmdRun[@]}"'
    echo >'"${strFl}.started"'
    if '$bRetryOnError';then
      while ! "${acmdRun[@]}";do echo "failed error $?";sleep 3;done
    else
      "${acmdRun[@]}";
    fi
    ' >"$strFl"
    
  while true;do
    bForceIgnoreStart=false
    
    if $bUseBashAutoCmdMode && $bSubShell;then
      bSubShell=false
      echo "WARN: disabling subshell mode to let bUseBashAutoCmdMode work for now." >&2
    fi
    
    if $bSubShell;then
      strCmd="bash -c ${strFl}"
    else
      # can the command passed thru qdbus param cause some problem if it is too complex? `bash -c` above is to prevent that.
      strCmd="`declare -p acmdRun`;"'"${acmdRun[@]}"'
      bForceIgnoreStart=true
    fi
    
    if $bUseBashAutoCmdMode;then
      #echoc --alert "@{-n}execute_command_by_uuid was @nremoved@{-n}, therefore this script wont work that way anymore :(";
      #exit 1
      strExecFile="`bashAutoCmdOnStart.sh --getExecFileForUID "$strTabUUID"`"
      declare -p strExecFile
      bWriteCmd=true
      while true;do
        if $bWriteCmd;then
          echo "$strCmd" >"$strExecFile"
          bWriteCmd=false
        fi
        #echo "INFO: Waiting '$strExecFile' be consumed..."
        : ${fWaitConsumeExecFile:=0.33} #help
        read -p "($(SECFUNCdtFmt --nodate --alt))INFO: Waiting (${fWaitConsumeExecFile}s) '$strExecFile' be consumed ([s]kip/[r]etry/...)`echo -ne "\r"`" -t $fWaitConsumeExecFile -n 1 strResp&&:;
        if [[ "$strResp" == "s" ]];then break;fi
        if [[ "$strResp" == "r" ]];then bWriteCmd=true;fi
        #sleep 1
        if [[ ! -f "$strExecFile" ]];then break;fi
      done
    else
      # this is for old guake version
      if [[ "$strTabUUID" == "USE_CURRENT_TAB" ]];then
        FUNCgrc execute_command "$strCmd"
      else
        FUNCgrc execute_command_by_uuid "$strTabUUID" "$strCmd"
      fi
    fi
    
    #sleep 1 #wait it start running
    
    : ${bWaitStart:=true} #help wait the command start running before sending the next one
    bForceRetry=false
    bKeepTmpExecFile=false
    bReallyWaitStart=$bWaitStart;if $bForceIgnoreStart;then bReallyWaitStart=false;fi
    if $bReallyWaitStart;then
      if $bForceCurrentTab || [[ "$strCurrentTabUUID" == "$strTabUUID" ]];then
        echo " >>>>>>>>>>>> "
        echo " >>>>> ATTENTION!!! this command '${acmdRun[*]}' was requested to be run on this current tab [$iCurrentTabIndex]'${strTabName}'($strCurrentTabUUID), so this script will not wait for the command to start and the related temporary exec file '${strFl}' will be kept."
        echo " >>>>>>>>>>>> "
        bKeepTmpExecFile=true
      else
				echo " >>>>>>>>>>>> "
				#echo "     > `date`: Waiting ($((nW++))) the command start running:"
				echo "     > `date`: Waiting the command start running:"
				echo "     > TAB: strTabName='${strTabName}',strTabUUID='$strTabUUID'"
				echo "     > CMD$iIdCmd: '${acmdRun[*]}'"
				echo "     > It may fail if there was some command typed and not executed yet there."
				echo "     > It may fail if there happened an input request there."
				echo "     > So you better check if no problem happened!"
				echo "     > But if no problem happened and you force a retry, it will be buffered and will execute again."
				#echo "     > Force retry run now? (y/...)"
				echo " >>>>>>>>>>>> "
        nW=0
        while [[ ! -f "${strFl}.started" ]];do
          strResp="$(read -p "$((nW++)):$(SECFUNCdtFmt --nodate --alt):Force retry run now? (y/...)" -t 3 -n 1 strResp;echo "$strResp")";echo #the script was stopping here... why did I have to subshell?
          if [[ "$strResp" == "y" ]];then echo " >>>>> Retrying";bForceRetry=true;break;fi
        done
      fi
    fi
    
    if ! $bForceRetry;then break;fi
  done
  
  : ${bRmTmpFiles:=false} #help delete temporary files
  if $bRmTmpFiles && ! $bKeepTmpExecFile;then rm -v "${strFl}" "${strFl}.started";fi
};export -f FUNCrunCmd

# run the commands
for iId in "${!astrIdList[@]}";do
  declare -p iId
  echo " Id: ${astrIdList[iId]}"
  
  bFound=false
  
  for iTab in "${!astrTabNameList[@]}";do
    strOpt="${astrOptList[iId]}"
    bRunOnThisTab=false
    if [[ "${astrTabNameList[iTab]}" == "${astrIdList[iId]}" ]];then bRunOnThisTab=true;fi
    if ((iTab==iCurrentTabIndex)) && [[ "$strOpt" =~ .*c.* ]];then bRunOnThisTab=true;fi
    if $bRunOnThisTab;then
      FUNCrunCmd "$strOpt" "${astrIdList[iId]}" "${astrTabUUIDList[iTab]}" $iId
      bFound=true
      break
    fi
  done
  
  : ${bAddTab:=true} #help if it doesnt exist yet
  if ! $bFound;then
    if $bAddTab;then
      FUNCgrc add_tab "$HOME"
      FUNCgrc rename_current_tab "${astrIdList[iId]}"
      astrTabNameList+=("${astrIdList[iId]}")
      strNewUUID="`FUNCgrc get_selected_uuidtab`"
      astrTabUUIDList+=("$strNewUUID")
      #FUNCrunCmd "${astrOptList[iId]}" "${astrIdList[iId]}" "USE_CURRENT_TAB" $iId
      FUNCrunCmd "${astrOptList[iId]}" "${astrIdList[iId]}" "$strNewUUID" $iId
    else
      echo " >>>>>>>>>>>> "
      echo " >>>>> WARNING: could not find a matching tab name for '${astrIdList[iId]}'"
      echo " >>>>>>>>>>>> "
    fi
  fi
done #for iId in "${!astrIdList[@]}";do

if $bGoBackToInitialTab;then FUNCgrc select_tab $iCurrentTabIndex;fi

#!/bin/bash

set -eu

source <(secinit)

if [[ "${1-}" == "--help" ]];then #help runs a daemon
  shift&&:
  egrep "[#]help" $0 >/dev/stderr
  exit
fi

export strId="`basename "$0"`"

if [[ "${1-}" == "--restore" ]];then #help restore the backup to current history and to the history file
  shift&&:
  strFl="`tempfile`";
  nDt="$(echo -n "$(date +"%s")")";
  cat ~/.bashHistorySpecialBackup.txt |sed -e "s'.*'#${nDt}\n&'" >"$strFl";
  
  cat "$strFl";
  if echoc -q "append to history?";then 
    cat "$strFl" >>"$HISTFILE"
    history -n;
  fi
  
  exit
fi

if [[ -n "${1-}" ]];then
  echoc -p "unrecognized option '${1-}'"
  $0 --help
  exit 1
fi

FUNC() {
  set -eu
  
  source <(secinit)

  SECFUNCuniqueLock --waitbecomedaemon --id "$strId"
  
  while true;do
    cat   "$HISTFILE" ~/.bashHistoryBkp.log ~/.bashHistorySpecialBackup.txt |egrep "#.*@BKP$" -a |sort -u >~/.bashHistorySpecialBackup.txt.NEW
    
    ls -l "$HISTFILE" ~/.bashHistoryBkp.log ~/.bashHistorySpecialBackup.txt ~/.bashHistorySpecialBackup.txt.NEW &&:
    wc -l "$HISTFILE" ~/.bashHistoryBkp.log ~/.bashHistorySpecialBackup.txt ~/.bashHistorySpecialBackup.txt.NEW &&:
    
    if ! cmp ~/.bashHistorySpecialBackup.txt.NEW ~/.bashHistorySpecialBackup.txt;then
      echoc --info "CHANGES FOUND!"
      trash -v ~/.bashHistorySpecialBackup.txt &&:
      mv -v ~/.bashHistorySpecialBackup.txt.NEW ~/.bashHistorySpecialBackup.txt
    else
      echoc --info "nothing changed"
      trash -v ~/.bashHistorySpecialBackup.txt.NEW
    fi
    
    echoc -w -t $((60*30)) "backup/check again now"
  done
};export -f FUNC

if [[ "$TERM" == dumb ]];then
  xterm -title "$strId" -e bash -c "FUNC"&
else
  FUNC
fi

#!/bin/bash
# Copyright (C) 2022 by Henrique Abdalla
#
# This file is part of ScriptEchoColor.
#
# ScriptEchoColor simplifies Linux terminal text colorizing, formatting 
# and several steps of script coding.
#
# ScriptEchoColor is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# ScriptEchoColor is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ScriptEchoColor. If not, see <http://www.gnu.org/licenses/>.
#
# Homepage: http://scriptechocolor.sourceforge.net/
# Project Homepage: https://sourceforge.net/projects/scriptechocolor/

source <(secinit)

#COPY FROM HERE: for simple scripts
declare -p SECstrUserScriptCfgPath
strExample="DefaultValue"
bExample=false
bExitAfterConfig=false
CFGstrTest="Test"
CFGstrSomeCfgValue=""
astrRemainingParams=()
astrAllParams=("${@-}") # this may be useful

SECFUNCcfgReadDB ########### AFTER!!! default variables value setup above, and BEFORE the skippable ones!!!

: ${strPath:="."} #help
: ${bWriteCfgVars:=true} #help false to speedup if writing them is unnecessary
: ${strEnvVarUserCanModify:="test"}
export strEnvVarUserCanModify #help this variable will be accepted if modified by user before calling this script
export strEnvVarUserCanModify2 #help test

while ! ${1+false} && [[ "${1:0:1}" == "-" ]];do # checks if param is set
	SECFUNCsingleLetterOptionsA;
	if [[ "$1" == "--help" ]];then #help show this help
		SECFUNCshowHelp --colorize "\tShows physical drives/devices(stripes/mirrors) related to specified path or file (or current path)."
    SECFUNCshowHelp --colorize "\tPARAMS: [strPath...] if nothing will be current path. Can be many ex.: /media/$USER/*"
		SECFUNCshowHelp --colorize "\tConfig file: '`SECFUNCcfgFileName --get`'"
		echo
		SECFUNCshowHelp
		exit 0
	elif [[ "$1" == "-c" || "$1" == "--configoption" ]];then #help <CFGstrSomeCfgValue> MISSING DESCRIPTION
		shift;CFGstrSomeCfgValue="${1-}"
		bExitAfterConfig=true
	elif [[ "$1" == "-e" || "$1" == "--exampleoption" ]];then #help <strExample> MISSING DESCRIPTION
		shift;strExample="${1-}"
	elif [[ "$1" == "-b" || "$1" == "--simpleBooleanSwitchOptionTrue" ]];then #help MISSING DESCRIPTION
		bExample=true
	elif [[ "$1" == "-B" || "$1" == "--simpleBooleanSwitchOptionFalse" ]];then #help MISSING DESCRIPTION
		bExample=false
	elif [[ "$1" == "-v" || "$1" == "--verbose" ]];then #help shows more useful messages
		SECbExecVerboseEchoAllowed=true #this is specific for SECFUNCexec, and may be reused too.
	elif [[ "$1" == "--cfg" ]];then #help <strCfgVarVal>... Configure and store a variable at the configuration file with SECFUNCcfgWriteVar, and exit. Use "help" as param to show all vars related info. Usage ex.: CFGstrTest="a b c" CFGnTst=123 help
		shift
		pSECFUNCcfgOptSet "$@";exit 0;
	elif [[ "$1" == "--" ]];then #help params after this are ignored as being these options, and stored at astrRemainingParams. TODO explain how it will be used
		shift #astrRemainingParams=("$@")
		while ! ${1+false};do	# checks if param is set
			astrRemainingParams+=("$1")
			shift&&: #will consume all remaining params
		done
	else
		echoc -p "invalid option '$1'"
		#"$SECstrScriptSelfName" --help
		$0 --help #$0 considers ./, works best anyway..
		exit 1
	fi
	shift&&:
done
# IMPORTANT validate CFG vars here before writing them all...
if $bWriteCfgVars;then SECFUNCcfgAutoWriteAllVars;fi #this will also show all config vars
if $bExitAfterConfig;then exit 0;fi

### collect required named params
# strParam1="$1";shift
# strParam2="$1";shift

# Main code
if SECFUNCarrayCheck -n astrRemainingParams;then :;fi

# SECFUNCuniqueLock --waitbecomedaemon # if a daemon or to prevent simultaneously running it

#VALIDATE ALL PARAMS HERE
if [[ -z "$strExample" ]];then echoc -p "invalid strExample='$strExample'";exit 1;fi

# List filler helper
#IFS=$'\n' read -d '' -r -a astrFlList < <(find)&&: #inside <() use any command that creates a list, one item per line

#COPY TO HERE: for simple scripts

tabs -8&&:

#if [[ -n "${1-}" ]];then
  #strPath="$1"
  #shift&&:
#fi

for strPath in "$@";do
  SECFUNCdrawLine --left " WORKING WITH: $strPath "
  
  if [[ ! -d "$strPath" ]] && [[ ! -f "$strPath" ]];then
    echo "ERROR: invalid path '$strPath'"
    exit 1
  fi

  #echo "$(realpath "$strPath") #REALPATH"
  strRealPath="$(realpath "$strPath")"

  strFS="`df "$strPath" |tail -n 1 |awk '{print $1}'`"; #declare -p strFS;
  strMountedAt="`df "$strPath" |tail -n 1 |awk '{print $6}'`"; #declare -p strMountedAt;
  echo "strRealPath='$strRealPath' strFS='$strFS' strMountedAt='$strMountedAt'"

  strFlFUNCworkPartsInTmp="`mktemp`"
  strFlFUNCworkPartsOutTmp="`mktemp`"
  function FUNCworkParts() {
      IFS=$'\n' read -d '' -r -a astrPartList < <(cat "$strFlFUNCworkPartsInTmp" |sort -u)&&:
      for strPart in "${astrPartList[@]}";do
        if [[ -n "$strPart" ]];then
          echo -e "${strPart}\t$strDev\t${strBlock-}" >>"$strFlFUNCworkPartsOutTmp"
          
          strPDChk="$( echo "$strPart" |sed -r -e "s'^[^-]*-(.*)'\1'" -e "s'(.*)-part.*$'\1'" )"
          bAdd=true
          if [[ -n "${astrPhysDrives[@]}" ]];then
            for((i=0;i<"${#astrPhysDrives[*]}";i++));do
              if [[ "${astrPhysDrives[i]}" == "$strPDChk" ]];then bAdd=false;break;fi
            done
          fi
          if $bAdd;then astrPhysDrives+=("$strPDChk");fi
        fi
      done
  }

  function FUNCfind() {
  #  find /dev/disk/by-id/ -name "*-part*" -lname "*/${1}" -exec ls -l '{}' \;
    strDev="$1"
    #find /dev/disk/by-id/ -name "*-part*" -lname "*/${strDev}" -exec readlink '{}' \; |grep -v "wwn-0x" |cut -d "/" -f 5
    find /dev/disk/by-id/ -name "*-part*" -lname "*/${strDev}" |grep -v "wwn-0x" |cut -d "/" -f 5 |tee "$strFlFUNCworkPartsInTmp" >/dev/null
    #strResult="$(find /dev/disk/by-id/ -name "*-part*" -lname "*/${strDev}")"
    #if [[ -n "$strResult" ]];then
      #echo "$strResult" |grep -v "wwn-0x" |cut -d "/" -f 5 |tee "$strFlFUNCworkPartsInTmp" >/dev/null
    #else
      #: # loop?
    #fi
  }

  #function FUNCexec() {
    #echo "EXEC: $@" >/dev/stderr
    #"$@"
  #}

  strDM="$(readlink "$strFS")"&&:
  if [[ -n "$strDM" ]];then
    echo
    #echo "LVM detected: $strDM"
    #declare -p strDM;

    strMainBlock="$(basename "$(find /dev/block -lname "$strDM")")"; #declare -p strMainBlock;
    if [[ -z "$strMainBlock" ]];then exit;fi

    #astrBlocks=($(sudo dmsetup ls --tree |tr "\n" "," |sed 's"[^0-9,:a-zA-Z()+_-]""g' |egrep "[(]${strMainBlock}[0-9:(),]*" -o |tr -d "()" |tr "," "\n" |tail -n +2));declare -p astrBlocks
    #|egrep "[(]${strMainBlock}[0-9:(),]*" -o 
    astrBlocks=( $(
      SECFUNCexecA -ce sudo dmsetup ls --tree \
        |sed -r -e 's"^ "@&"' \
        |sed -r '/^@/ s,.*([(].*[)]),\1,' \
        |tr "\n" "," \
        |egrep "[(]${strMainBlock}[)],[0-9:(),]*" -o \
        |tr -d "()" \
        |tr "," "\n" \
        |tail -n +2 
    ) )
    #declare -p astrBlocks
    echoc --info "LVM detected:@{-a} strDM='$strDM' strMainBlock='$strMainBlock' astrBlocks=(${astrBlocks[@]})"

    echo
    echo -e "Partition\tdevice\tblock"
    astrPhysDrives=()
    for strBlock in "${astrBlocks[@]}";do
      strDev="$(basename $(find /dev/ -name "$strBlock" -exec readlink '{}' \; 2>&1 |grep -v "Permission denied"))" 2>/dev/null &&:
      if [[ "$strDev" =~ loop.* ]];then
        strLoopFile="$(losetup -l |grep "/dev/$strDev" |awk '{print $6}')"
        #declare -p strLoopFile
        echoc --info "LOOP FILE DETECTED:@{-a} strLoopFile='$strLoopFile' Looking for details about where this loop file is stored."
        $0 "$strLoopFile"
        exit
      else
    #    find /dev/disk/by-id/ -name "*-part*" -lname "*/$strDev" -exec ls -l '{}' \; |grep -v "wwn-0x" |egrep "/dev/disk/by-id/[^ ]*" -o |tr "/" " " |awk '{print $4}' |tee "$strFlFUNCworkPartsInTmp" >/dev/null
        #find /dev/disk/by-id/ -name "*-part*" -lname "*/$strDev" |grep -v "wwn-0x" |cut -d "/" -f 5 |tee "$strFlFUNCworkPartsInTmp" >/dev/null
        FUNCfind "$strDev"
        FUNCworkParts
      fi
    done
  else
    strDev="../`basename "$strFS"`";declare -p strDev
    #find /dev/disk/by-id/ -name "*-part*" -lname "*/$strDev" -exec ls -l '{}' \; |tee "$strFlFUNCworkPartsInTmp" >/dev/null
    #find /dev/disk/by-id/ -name "*-part*" -lname "*/${strDev}" |grep -v "wwn-0x" |cut -d "/" -f 5 |tee "$strFlFUNCworkPartsInTmp" >/dev/null
    FUNCfind "$strDev"
    FUNCworkParts
  fi
  cat "$strFlFUNCworkPartsOutTmp" |sort -u

  echo
  echo "PhysicalDrive(s):"
  for((i=0;i<"${#astrPhysDrives[*]}";i++));do
    echo "${astrPhysDrives[i]}"
  done |sort
  
  echo
done
